import {DashboardComponent} from './dashboard.component';
import {IsLoggedInGuard} from '../shared/guards/is-loggedin/is-loggedin.guard';
import {HomeComponent} from './home/home.component';
import {UserComponent} from './user/user.component';
import {IsAdminGuard} from '../shared/guards/is-admin/is-admin.guard';

export const DashboardRoutes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [IsLoggedInGuard],
    children: [
      {path: '', redirectTo: '/home', pathMatch: 'full'},
      {path: 'home', component: HomeComponent},
      {
        path: 'user',
        component: UserComponent,
        canActivate: [IsAdminGuard]
      }
    ],
  },
];

