import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';


@Injectable({
    providedIn: 'root'
})
export class AuthService {

  constructor(
    private cookieService: CookieService,
    private router: Router,
  ){}
  login(data: { username: string, password: string }) {
    if(data.username &&  data.password){
      this.cookieService.set('access_token','test-token-in-cookie');
      this.router.navigateByUrl('/');
    }
  }
}
