import {LoginComponent} from './login/login.component';
import {AuthComponent} from './auth.component';
import {Routes} from '@angular/router';
import {HasProviderGuard} from '../app/shared/guards/has-provider/has-provider.guard';

export const AuthRoutes: Routes = [
  {
    path: '',
    component: AuthComponent,
    canActivate: [
      HasProviderGuard
    ],
    children: [
      {path: 'login', component: LoginComponent},
    ],
  },
];

