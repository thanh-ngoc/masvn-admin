import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../app/shared/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user = {
    email: '',
    password: '',
    is_remember: false,
  };
  isError = false;
  errors: any[];
  loading: boolean;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  login(): void {
    this.loading = true;

    const data = {
      username: this.user.email,
      password: this.user.password
    };
    this.authService.login(data);
  }
}
